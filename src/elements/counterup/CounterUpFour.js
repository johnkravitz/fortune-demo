import React from 'react';
import CountUp from 'react-countup';
import TrackVisibility from "react-on-screen";
import {useTranslation} from "react-i18next";

const CounterUpFour = ({textALign, counterStyle, column}) => {
    const {t} = useTranslation();
    const Data = [
        {
            countNum : 500,
            countTitle: t("Happy-Clients"),
        },
        {
            countNum : 20,
            countTitle: t("Employees"),
        },
        {
            countNum : 25,
            countTitle: t("Conference"),
        },
        {
            countNum : 200000,
            countTitle: t("Total-Users"),
        },
    ];
    return (
        <div className="row">
            {Data.map((data, index) => (
                <div className={`${column}`} key={index}>
                    <div className={`count-box ${counterStyle} ${textALign}`}>
                        <TrackVisibility once>
                            {({ isVisible }) => isVisible && 
                                <div className="count-number">{isVisible ? <CountUp end={data.countNum} /> : 0}</div>}
                        </TrackVisibility>
                        <h5 className="counter-title">{data.countTitle}</h5>
                    </div>
                </div>
            ))}
        </div>
    )
}
export default CounterUpFour;