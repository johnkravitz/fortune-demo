import React from 'react';
import {Link} from "react-router-dom";
import CalltoActionSeven from "../../elements/calltoaction/CalltoActionSeven";
import footerOne from "../../data/footer/footerOne.json";
import ScrollTop from "./ScrollTop";
import { FiFacebook, FiTwitter, FiInstagram, FiLinkedin } from "react-icons/fi";
import {useTranslation} from "react-i18next";

const FooterTwo = () => {
    const {t} = useTranslation();
            const PopupData1 = [
        
                {
                    "id": 1,
                    "text": t("Diseño-Corporativo"),
                    "url": "/service"
                },
                {
                    "id": 2,
                    "text": t("Marketing-Online"),
                    "url": "/service"
                },
                {
                    "id": 3,
                    "text": t("Racebook-Online"),
                    "url": "/service"
                },
                {
                    "id": 4,
                    "text": t("Asesoria-Legal"),
                    "url": "/service"
                }
        
            ]
            const PopupData2 = [
                {
                    "id": 1,
                    "text": t("products"),
                    "url": "/gallery"
                },
                {
                    "id": 2,
                    "text": "Whitelabels",
                    "url": "#wl"
                }
            ]

            const PopupData3 = [    

                {
                    "id": 1,
                    "text": t("about-us"),
                    "url": "/about-us"
                },
                {
                    "id": 2,
                    "text": t("Licence"),
                    "url": "/about-us"
                },
                {
                    "id": 4,
                    "text": t("news"),
                    "url": "/about-us"
                }
            ]
            const PopupData4 = [    
    
                {
                    "id": 1,
                    "text": t("Live-Slots"),
                    "url": "/gallery"
                },
                {
                    "id": 2,
                    "text": t("Terminales"),
                    "url": "/gallery"
                },
                {
                    "id": 3,
                    "text": t("Loteria-en-Vivo"),
                    "url": "/gallery"
                }
            ]
        
    return (
        <>
            <footer className="rn-footer footer-style-default variation-two">
                <CalltoActionSeven />
                <div className="footer-top">
                    <div className="container">
                        <div className="row">
                            {/* Start Single Widget  */}
                            <div className="col-lg-2 col-md-6 col-sm-6 col-12">
                                <div className="rn-footer-widget">
                                    <h4 className="title">{t("services")}</h4>
                                    <div className="inner">
                                        <ul className="footer-link link-hover">
                                            {PopupData1.map((data, index) => (
                                                <li key={index}><a href={`${data.url}`}>{data.text}</a></li>
                                            ))}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            {/* End Single Widget  */}

                            {/* Start Single Widget  */}
                            <div className="col-lg-2 col-md-6 col-sm-6 col-12">
                                <div className="rn-footer-widget">
                                    <div className="widget-menu-top">
                                        <h4 className="title">{t("Solutions")}</h4>
                                        <div className="inner">
                                            <ul className="footer-link link-hover">
                                                {PopupData2.map((data, index) => (
                                                    <li key={index}><a href={`${data.url}`}>{data.text}</a></li>
                                                ))}
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/* End Single Widget  */}

                            {/* Start Single Widget  */}
                            <div className="col-lg-2 col-md-6 col-sm-6 col-12">
                                <div className="rn-footer-widget">
                                    <h4 className="title">{t("about-us")}</h4>
                                    <div className="inner">
                                        <ul className="footer-link link-hover">
                                            {PopupData3.map((data, index) => (
                                                <li key={index}><a href={`${data.url}`}>{data.text}</a></li>
                                            ))}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            {/* End Single Widget  */}

                            {/* Start Single Widget  */}
                            <div className="col-lg-2 col-md-6 col-sm-6 col-12">
                                <div className="rn-footer-widget">
                                    <h4 className="title">{t("Resources")}</h4>
                                    <div className="inner">
                                        <ul className="footer-link link-hover">
                                            {PopupData4.map((data, index) => (
                                                <li key={index}><a href={`${data.url}`}>{data.text}</a></li>
                                            ))}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            {/* End Single Widget  */}

                            {/* Start Single Widget  */}
                            <div className="col-lg-4 col-md-6 col-sm-6 col-12">
                                <div className="rn-footer-widget">
                                    <h4 className="title">{t("stay-with-us")}</h4>
                                    <div className="inner">
                                        <h6 className="subtitle">{t("footer-subtitle")}</h6>
                                        <ul className="social-icon social-default justify-content-start">
                                        <li><a href="https://www.facebook.com/fortunecom" target='_blank' rel="noreferrer"><FiFacebook /></a></li>
                                        <li><a href="https://www.instagram.com/fortune/" target='_blank' rel="noreferrer"><FiInstagram /></a></li>
                                        <li><a href="https://www.linkedin.com/company/fortune" target='_blank' rel="noreferrer"><FiLinkedin /></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            {/* End Single Widget  */}
                        </div>
                    </div>
                </div>
            </footer>
            <ScrollTop />
        </>
    )
}

export default FooterTwo;
