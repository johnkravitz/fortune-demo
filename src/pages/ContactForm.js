import React from 'react';
import axios from 'axios';

class ContactForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      phone: '',
      message: ''
    }
  }

  handleSubmit(e){
    e.preventDefault();
    axios({
      method: "POST",
      url:"https://mail.fortune.net/api/contact/send-mail",
      data:  this.state
    }).then((response)=>{
      if (response.data) {
        alert(response.data);
        this.resetForm()
      } else if (!response.data) {
        alert("No hay mensaje")
      }
    })
  }

  resetForm(){
    this.setState({name: '', email: '', phone: '', message: ''})
  }

  render() {
    return(
      <div className="App" style={{maxWidth:'390px', margin:'auto'}}>
        <form id="contact-form" onSubmit={this.handleSubmit.bind(this)} method="POST">
          <div className="form-group">
              <label htmlFor="name">Name</label>
              <input type="text" className="form-control" id="name" value={this.state.name} onChange={this.onNameChange.bind(this)} />
          </div>
          <div className="form-group">
              <label htmlFor="exampleInputEmail1">Email address</label>
              <input type="email" className="form-control" id="email" aria-describedby="emailHelp" value={this.state.email} onChange={this.onEmailChange.bind(this)} />
          </div>
          <div className="form-group">
              <label htmlFor="exampleInputEmail1">Phone</label>
              <input type="phone" className="form-control" id="phone" aria-describedby="phoneHelp" value={this.state.phone} onChange={this.onPhoneChange.bind(this)} />
          </div>
          <div className="form-group">
              <label htmlFor="message">Message</label>
              <textarea className="form-control" rows="5" id="message" value={this.state.message} onChange={this.onMessageChange.bind(this)} />
          </div>
          <button type="submit" className="btn btn-primary">Submit</button>
        </form>
      </div>
    );
  }

  onNameChange(event) {
	  this.setState({name: event.target.value})
  }

  onEmailChange(event) {
	  this.setState({email: event.target.value})
  }

  onPhoneChange(event) {
	  this.setState({phone: event.target.value})
  }

  onMessageChange(event) {
	  this.setState({message: event.target.value})
  }
}

export default ContactForm;